# moodle-auth-tester

This is a python script to authenticate to a moodle website and a bash script to parallelize authentication using a user list.

For now it's commented in French, i'll translate later

Basically, you fill user.txt with usernames and passwords separated by tabulations, 

(it's in cleartext, don't do that with actual users, it's only for testing)

then you change the hostname from the script with your moodle's.

Now just run parallel-auth-launcher.sh and the script will try all lines from users.txt

