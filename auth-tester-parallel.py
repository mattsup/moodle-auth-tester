#! /usr/bin/python
# -*- coding: utf-8 -*-
### ancien encodage : iso-8859-1


# Script de connexion à ent 
# Pré requis:
#    python 2.4
#    Installer 
#          wget http://peak.telecommunity.com/dist/ez_setup.py
#          python ez_setup.py
#          easy_install mechanize
#####
###  Et installer également StatsD :  
###        git clone https://github.com/WoLpH/python-statsd.git
###        cd python-statsd
###        python2 setup.py install
#####
#    Renseigner un user et un mot de passe pour la connexion, nom de votre machine
#    Pour débugger positionner b.set_debug_http(True)


from collections import defaultdict
import re,sys
from mechanize import Browser
from datetime import datetime
import itertools
import socket
import time
import statsd
from timeit import Timer

### Librairies non utilisées :
### import smtplib,sys
### from email.MIMEText import MIMEText



def runParcours():

    ### Change hostname or IP here for now
    hostname="10.0.2.15"


    ### URLs à renseigner :
    ### urllogin = "https://" hostname "/moodle/login/index.php"
    urllogin = "http://" + hostname + "/moodle/login/index.php"
    urlmoodle = "http://" + hostname + "/moodle/"
    urlfds="https://" + hostname + "/moodle/course/view.php?id=2"
    
    ### Utilisateur à tester (à renseigner en paramètre du script) :
    username = sys.argv[1]
    password = sys.argv[2]

    print 'Utilisateur en cours :'
    print username


    ### Initialisation du navigateur pour lancer les requetes 
    b = Browser()
    b.set_handle_robots(False)
    b.set_proxies({ })
    
    ### On renseigne le User agent qu'on veut montrer à Moodle
    
    ###    b.addheaders = [("User-agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; fr-FR;rv:1.7.12) Gecko/20050919 Firefox/1.0.7"),]
    b.addheaders = [("User-agent", "Mozilla/5.0 (X11; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0"),]
    
    
    ### Phase de tests :
    
    url=urllogin
    
    try:
    
        b.set_debug_http(True)
        b.set_debug_redirects(True)
        b.set_handle_redirect(True)
    
   
        b.open(url)
        b.select_form(nr=0)
        b.form.set_all_readonly(False)
        
        b['username'] = username 
        b['password'] = password
        
    
        reponse = b.submit()
        ### response=b.open(urlmoodle)
        ### response=b.open(urlfds)
        ### for r in response.readlines():
        ###    print(r)
   


    ### Définition des exceptions :
 
    except SystemExit:
        pass
    
    except:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        mon_erreur = str(exc_type) + " " + str(exc_value) + " *** url de tests: " + url
        print(mon_erreur)



### Init des paramètres de connexion pour StatsD
statsd.Connection.set_defaults(host=hostname, port=80, sample_rate=1, disabled=False)

### Début du chrono
timer = statsd.Timer('TestMoodle')
timer.start()

### Lancement du script
runParcours()

### Fin du chrono
timer.stop('Timertest')


